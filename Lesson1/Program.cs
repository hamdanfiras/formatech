﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson1
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime bd = new DateTime(1980, 4, 4);
            DateTime nextBd = new DateTime(DateTime.Today.Year, bd.Month, bd.Day);

            if (nextBd < DateTime.Today)
            {
                nextBd = new DateTime(nextBd.Year + 1, nextBd.Month, nextBd.Day);
            }

            // Dog max = new Dog();
            // Dog max = new Dog("Max");

            DateTime today = DateTime.Today;

            bool isMyBirthday = (bd.Month == today.Month) && (bd.Day == today.Day);
            TimeSpan ts = nextBd - today;

            if (isMyBirthday)
            {
                Console.WriteLine("Happy birthday");
            }
            else if (ts.Days == 1)
            {
                Console.WriteLine("I am so excited, bd is tomorrow");
            }
            else
            {
                Console.WriteLine("Have a good day");
            }

            Console.ReadLine();

        }

        // intro to bool
        static void Main3(string[] args)
        {
            int x = 6;
            int y = 5;
            bool b = x == y;
            bool theOppositeOfB = !b;
            bool xNotEqualToY = x != y;


            bool xBiggerThanY = x > y;
            bool xBiggerOrEqualToY = x >= y;

            if (x == y)
            {
            }


        }

        //  simple types
        static void Main2(string[] args)
        {
            int x = 6;
            int y = 2;
            int z = x * y;

            string s = "Jimmy";
            string n = s + " Page";
            Console.WriteLine(n);

            decimal xy = 6.78m;

            decimal d = (decimal)x;
            int o = (int)d;

            string p = "33";
            int p2 = int.Parse(p);

            char c = 'a';
            char j = s[0];

            Console.WriteLine(z);
            Console.WriteLine(s[0]);
            Console.ReadLine();
        }
    }
}
